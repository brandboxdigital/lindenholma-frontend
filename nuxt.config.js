export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: "server",
  // ssr: true,

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: "Lindenholma",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Piepildi savu ikdienu ar komfortu un ērtībām, ko piedāvā Lindenholma dzīvojamais kvartāls Mārupē! Izvēlies kādu no 209 dzīvokļiem jau šodien!" },
      {
        name: "facebook-domain-verification",
        content: "49fykpx8jf2vw8vsnqdal8ldi0dyck"
      },
      {
        name: "google-site-verification",
        content: "a4G2hdLq0NVxIJLqlt_V2-JRnoC1Uk08rg3meOuPZ0I"
      },
      { hid: "og:title", property:"og:title", content:"Mūsdienīgs dzīvojamais kvartāls Mārupē | Lindenholma" },
      { hid: "og:description", property:"og:description", content: "Piepildi savu ikdienu ar komfortu un ērtībām, ko piedāvā Lindenholma dzīvojamais kvartāls Mārupē! Izvēlies kādu no 209 dzīvokļiem jau šodien!" },
			{ hid: "og:image", property:"og:image", content: "/images/apartmentsPage/landing.png" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" }
      // { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css' }
    ],
    script: [
      {
        src: "https://www.googletagmanager.com/gtag/js?id=G-XKP7QMTLE7",
        async: true
      },
      {
        src: "/ga4.js"
      }
    ]
  },


  publicRuntimeConfig: {
    APP_DOMAIN: process.env.APP_DOMAIN || "lindenholma.lv",
    DISABLE__STATICALLY: process.env.DISABLE__STATICALLY || false,
  },

  //
  //

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    "@/assets/styles/main.scss",
    "vue-slick-carousel/dist/vue-slick-carousel.css"
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: "./plugins/vue-slick-carousel.js" },
    // { src: "./plugins/swiper.js", mode: "client" },
    { src: "./plugins/axios.js" },
    { src: "./plugins/globalFilters.js" },
    //{ src: "~/plugins/vue-lazyload.js", mode: "client" },
    { src: "~/plugins/vue-lazy-image.js" },

    { src: "~/plugins/leaflet-gesture-handling.js", mode: "client" }
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: {
    dirs: [
      "~/components",
      "~/components/base",
      "~/components/partials",
      "~/components/assets",
      // "~/components/widgets"
    ]
  },

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    // '@nuxtjs/tailwindcss',

    "@nuxtjs/style-resources"
  ],

  styleResources: {
    scss: [
      "@/assets/_variables.scss"
    ]
  },

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    "nuxt-i18n",
    "bootstrap-vue/nuxt",
    //https://github.com/nuxt-community/gtm-module
    "@nuxtjs/gtm",
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    "nuxt-leaflet",
    "nuxt-user-agent",
    "nuxt-mq",
    "@nuxtjs/sitemap",
    "nuxt-ssr-cache",
    ['nuxt-buefy', { css: false }],
  ],

  // nuxt-i18n module configuration (https://i18n.nuxtjs.org/basic-usage)
  i18n: {
    defaultLocale: "lv",
    strategy: "prefix",
    locales: [
      {
        code: "lv",
        file: "get.js",
        name: "Latviski",
        iso: "lv-LV",
        isCatchallLocale: true
      },
      { code: "ru", file: "get.js", name: "По-русски", iso: "ru-RU" },
      { code: "en", file: "get.js", name: "English", iso: "en-US" }
    ],
    lazy: true,
    langDir: "locales/",
    seo: true,

    detectBrowserLanguage: false,
  },

  // sitemap: {
  //   hostname: 'https://lindenholma.lv',
  //   i18n:  {
  //     locales: ['lv', 'ru', 'en'],
  //     routesNameSeparator: '___'
  //   }
  // },

  mq: {
    defaultBreakpoint: "lg",
    breakpoints: {
      sm: 950,
      lg: Infinity
    }
  },

  //https://github.com/nuxt-community/gtm-module#options
  /**
   * Lindenholma uses GA4
   * gtm-module currently does not support GA4
   * @see https://github.com/nuxt-community/gtm-module/issues/82
   */
  gtm: {
    id: "GTM-MSSFD4B", // Used as fallback if no runtime config is provided
    enabled: true
  },

  bootstrapVue: {
    bootstrapCSS: false, // Or `css: false`
    bootstrapVueCSS: false // Or `bvCSS: false`
  },

  axios: {
    baseURL: process.env.BACKEND_URL || "https://admin.lindenholma.lv/api/v1/"
    // baseURL: "http://lindenholma-cms.test/api/v1"
    // https://lindenholma.brandbox.digital/api/v1/
    // https://admin.lindenholma.lv/api/v1/
  },

  version: 1.0,
  cache: {
    // if you're serving multiple host names (with differing
    // results) from the same server, set this option to true.
    // (cache keys will be prefixed by your host name)
    // if your server is behind a reverse-proxy, please use
    // express or whatever else that uses 'X-Forwarded-Host'
    // header field to provide req.hostname (actual host name)
    useHostPrefix: false,
    pages: [
      // these are prefixes of pages that need to be cached
      // if you want to cache all pages, just include '/'
      '/',

      // you can also pass a regular expression to test a path
      // /^\/page3\/\d+$/,

      // to cache only root route, use a regular expression
      // /^\/$/
    ],

    // key(route, context) {
      // custom function to return cache key, when used previous
      // properties (useHostPrefix, pages) are ignored. return
      // falsy value to bypass the cache
    // },

    store: {
      type: 'memory',

      // maximum number of pages to store in memory
      // if limit is reached, least recently used page
      // is removed.
      max: 50,

      // number of seconds to store this page in cache
      ttl: 60 * 60, // 1 hour
    },
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    loaders: {
      scss: {
        implementation: require('sass'),
        sassOptions: {
          api: "legacy",
          silenceDeprecationWarnings: true,
          silenceDeprecations: ['legacy-js-api'],
        }
      },
    },
  },

  server: {
    port: process.env.PORT || 3000, // default: 3000,
    host: process.env.HOST || "0.0.0.0" // default: localhost
  },

};
