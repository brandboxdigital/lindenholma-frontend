# Build
FROM node:18-alpine3.20 as builder
WORKDIR /app
RUN apk --no-cache add openssh g++ make python3 git
COPY package.json /app/
RUN npm install && npm cache clean --force
ADD . /app
RUN npm run build

# Run
FROM node:18-alpine3.20
WORKDIR /app
COPY --from=builder /app /app
EXPOSE 3000
CMD ["npm", "start"]