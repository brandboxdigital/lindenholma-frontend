import _ from 'lodash';

function objectToFormData(obj, form, namespace) {

  var fd = form || new FormData();
  var formKey;

  for(var property in obj) {
    if(obj.hasOwnProperty(property)) {

      if(namespace) {
        formKey = namespace + '[' + property + ']';
      } else {
        formKey = property;
      }

      // if the property is an object, but not a File,
      // use recursivity.
      if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {

        objectToFormData(obj[property], fd, property);

      } else {

        // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }

    }
  }

  return fd;

};

export default {

  methods: {

    formAxiosPromise(url, models, hasFile) {

      if (hasFile) {
        // Not complete :((((((
        const form = objectToFormData(models);

        return this.$axios.post(url, form, {
          headers: {
              'Content-Type': 'multipart/form-data'
          }
        });

      } else {

        const data = _.mapValues(models, 'value');

        return this.$axios.post(url, data, {
          headers: {
              'Content-Type': 'application/json'
          }
        });
      }
    },
    formNewUrlModel() {
      return {
        value: this.$route.query,
        message: null,
      }
    },
    formNewModel() {
      return {
        value: null,
        message: null,
      };
    },

    formNewLanguageModel() {
      return {
        value: this.$i18n.locale,
        message: null,
      };
    },

    formReset(fields) {
      _.forEach(fields, function(model, key) {
        model = this.formNewModel();
      });
    },

    formGetFieldType(field) {
      return (field.message == null) ? null : 'is-danger';
    },

    formGetFieldMessage(field) {
      return (field.message == null) ? null : field.message;
    },
    formGetFieldState(field) {
        return (field.message == null) ? true : false;
    },

    formSetFieldMessages(fields, data) {
      _.forEach(fields, function(model, key) {
        if ( data[key] ) {
          model.message = data[key].join(', ');
        } else {
          model.message = null;
        }
      });
    },
  }
}