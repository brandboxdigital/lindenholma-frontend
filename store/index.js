export const state = () => ({
    loading: true,
    // cursor: '',
    images: [],
    cursorEffects: false
})

export const mutations = {

  applyCursorEffects: (state,payload) => {
    state.cursorEffects = payload
  },
  loading: (state) => {
      state.loading = !state.loading
  },
  // setCursor: (state,payload) => {
  //   state.cursor = payload
  // },
  initImages: (state, images) => {
      state.images = images.data
  },

}

// export const actions = {
//   await loadImages ({commit, $axios}) {
//     const images = await $axios.$get('/v1/content/images');
//     commit('initImages', images);
//   }
// }
