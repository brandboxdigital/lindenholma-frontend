export default async ({$axios, language, app},locale) => {
    $axios.setHeader('Accept-Language', locale);
    const strings = await $axios.$get('/content/strings');
    // if (process.env.LOCK_LOCALES) {
    //   console.log('LOCKED STRINGS!!');
    //   let _strings = require(`~/locales/${locale}.json`);
    //   return _strings.data;
    // }

    return strings.data;
}
