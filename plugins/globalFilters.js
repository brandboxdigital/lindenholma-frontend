import Vue from "vue"

function ifExists(object, variable) {
  return (object != null
      && typeof object[variable] !== 'undefined');
}

function fStatically(url, width, height)  {
  if (process.server) return url

  if (!url) {
    return '';
  }

  if (typeof url === 'object') {
    if (Object.keys(url).indexOf('path') > -1) {
      url = url.path;
    } else {
      return '';
    }
  }

  // Check if we need to run Nuxt in development mode
  const isDev = process.env.NODE_ENV !== 'production'
  const isDisabled = $nuxt.$config.DISABLE__STATICALLY || false

  if (isDev || isDisabled) {
    return url
  }

  /**
   * Already is stacally link..
   */
  if (url.indexOf('ik.imagekit.io') !== -1) {
    return url;
  }

  let link   = url;
  let domain = "lindenholma.lv";

  try {
     domain = $nuxt.$config.APP_DOMAIN || "lindenholma.lv";
  } catch (error) {}

  if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
  } else {
    const noHttp = url.replace('http://','').replace('https://','');

    domain = noHttp.split(/[/?#]/)[0];
    link   = noHttp.replace(domain+"/", '');
  }

  try {
    domain = domain.replace('http://','').replace('https://','').replace(/\/$/, '');
  } catch (error) {}

  if (width && height) {
    return `https://ik.imagekit.io/bboxdigi/lindenholma/${link}?tr=c-at_max,w-${width},h-${height}`;
  } else if (width) {
    return `https://ik.imagekit.io/bboxdigi/lindenholma/${link}?tr=c-at_max,w-${width}`;
  } else {
    return `https://ik.imagekit.io/bboxdigi/lindenholma/${link}`;
  }

}

if (!Vue.__my_mixin__) {
  Vue.__my_mixin__ = true

  Vue.filter("grHtml", function(field)  {
    if (ifExists(field, 'html')) {
      return field['html'];
    }
    return null;
  });

  Vue.filter("grImgUrl", function(img)  {
    if (ifExists(img, 'url')) {
      return img['url'];
    }
    return null;
  });

  Vue.filter("year", function(date)  {
    return date.split(/[-?#]/)[0];
    // let dt = new Date(date);
    // return dt.getYear()
  });

  Vue.filter("statically", function(url, width, height)  {
    return fStatically(url, width, height);
  });

  Vue.filter("nl2br", function(doc)  {
    if (doc) {
      return doc.replace(/(\r)*\n/g, '<br>')
    }
    return null;
  });


  Vue.mixin({

    methods: {
      global_generatePrivacyPolicyLink: function (iswarranty) {
        const privacyTxt = (iswarranty) ? this.$t('modals.privacy_policy_warranty') : this.$t('modals.privacy_policy');

        const privacyLink = this.localePath('pivacy-policy');
        const a = `<a target="_blank" href="${privacyLink}">${privacyTxt}</a>`;
        return [a];
      },

      global_statically(url, width, height) {
        return fStatically(url, width, height);
      },

      global_fixSquareMeters(text) {
        if (text) {
          return text.replace(/м \*|m \*|m2|м2/g,this.$t('global.square_meters'));
        }

        return text;
      }
    },
  })

};

export default (context, inject) => {
  inject('imagekit', fStatically);
};
